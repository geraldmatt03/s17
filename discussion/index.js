// discussion
// [SECTION] - Introduction to Arrays

// practice: create/declare multiple variables
let student1 = 'Martin'; 
let student2 = 'Maria'; 
let student3 = 'John'; 
let student4 = 'Ernie'; 
let student5 = 'Bert';

// store the ff values inside a single container
// to declare an array, we simply use an 'array literal' or suare bracket '[]'
// use an '=' => assignment operator to repackage the structure inside the variable
// be careful when selecting variable names. some words are reserved with javascript
let batch = ['Martin', 'Maria', 'John', 'Ernie', 'Bert'];
console.log(batch);

// create an array that wil contain different computerBrands
let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer', 'Toshiba', 'Fujitsu'];
console.log(computerBrands);

/*
	SIDE LESSON
	'' or "" => both are used to declare string data type in JavaScript
	 => 'apple' === "apple"
	'apple' - correct
	"apple" - correct
	'apple" - incorrect

	How to choose?

	=> Use case of the data
		1. if you're going to use quotations, dialog inside a string.
			- if you will use single quotations when declaring such values like below, it will result to prematurely end the statement

			let message = 'Using Javascript's Array literals; === X

			let message = "Using JavaScript's Array literals"; === /

			let lastWords = '"I shall return" - MacArthur'; === /
	NOTE: when selecting the correct quotation type, you can use them as an [ESCAPE TOOL] to properly declare expressions within a string.

	ALTERNATIVE APPROACH => "\" forward slash to insert quotations
		ex.
			let message2 = 'Using JavaScript\'s Array literals';
*/
let message2 = 'Using JavaScript\'s Array literals';
console.log(message2);

//practicing the use of git branches